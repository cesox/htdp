# How to Design Programs, 2nd #

## Description ##

This repo. contains my solution for some of the excersises in [How to Design Programs (HtDP)](https://htdp.org/).

##### Doing:
* [14-18 Abstraction](./14-18_Abstraction/)

##### Done:
* [1-7 Fixed-Size Data](./1-7_Fixed_size_data/)
* [8-13 Arbitrarily Large Data](./8-13_Arbitrarily_large_data/)

##### TODO:
* 19-24 Intertwined Data
* 25-30 Generative Recursion
* 31-34 Accumulators



